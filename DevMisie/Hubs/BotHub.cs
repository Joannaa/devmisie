﻿using System.Threading.Tasks;
using System;
using DevMisie.Clients;
using DevMisie.Services;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using TTSSample;

namespace DevMisie.Hubs
{
    [HubName("botHub")]
    public class BotHub : Hub<IClient>
    {
        private ISpeechService _speechService;

        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public BotHub(ISpeechService speechService)
        {
            _speechService = speechService;
        }
        [HubMethodName("Speech")]
        public void SpeechToText(string msg)
        {
            _speechService.MicrophoneRecognition(msg);   
        }
        private readonly static Speech _speech = new Speech();
      // private static QuestionsManager _manager = new QuestionsManager();

        public void GetAudioFromText(string text)
        {
            try
            {
                var audio = _speech.GetSpeechStream(text);
                Clients.Caller.BotSendAudoToClient(audio);
            }
            catch (Exception)
            {
               
            }
        }
    }
}