﻿$(document).ready(function () {
    jQuery("#webcam")
        .webcam({
            width: 320,
            height: 240,
            mode: "save",
            swffile: "/Scripts/camera/jscam.swf", // canvas only doesn't implement a jpeg encoder, so the file is much smaller
            debug: function(type, status) {
                $('#camStatus').append(type + ": " + status + '<br /><br />');
            },
            onCapture: function () {  
                webcam.save("/Home/Capture");
            }  
        });

    setInterval(Capture, 500000);
});

function Capture() {
    webcam.capture();
}