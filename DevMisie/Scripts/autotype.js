﻿$(function () {
    $(".typing-container").typed({
        strings: ["First sentence.", "Second sentence."],
        typeSpeed: 0
    });
});