﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;
using DevMisie.Services;
using Microsoft.ProjectOxford.Face;

namespace DevMisie.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFaceDetectionService _faceDetectionService;
        private readonly INotificationSignalRService _notificationSignalRService;
        private readonly ISpeechService _speechService;
        private static int _numberOfDetection = 0;
        private static int _firstSeen = 0;

        public HomeController(IFaceDetectionService faceDetectionService, 
            INotificationSignalRService notificationSignalRService, ISpeechService speechService)
        {
            _faceDetectionService = faceDetectionService;
            _notificationSignalRService = notificationSignalRService;
            _speechService = speechService;
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Camera()
        {
            return View();
        }

        public ActionResult Welcome()
        {
            return View();
        }

        public ActionResult Statistics()
        {
            return View();
        }

        public async Task<ActionResult> Capture()
        {
            var stream = Request.InputStream;
            string dump;
            DateTime nm = DateTime.Now;

            string date = nm.ToString("yyyymmddMMss");
            var pathDirectory = Server.MapPath("~/WebImages/");
            var path = pathDirectory + date + "test.jpg";
            using (var reader = new StreamReader(stream))
            {
                dump = reader.ReadToEnd();
                if (!Directory.Exists(pathDirectory))
                {
                    Directory.CreateDirectory(pathDirectory);
                }
                System.IO.File.WriteAllBytes(path, String_To_Bytes2(dump));
            }
            var faceDetection = await _faceDetectionService.DetectFace(path);
            if (faceDetection)
            {
                _numberOfDetection += 1;
                _firstSeen += 1;
                if (_numberOfDetection == 1 && _firstSeen == 1)
                {
                    _notificationSignalRService.BotSayToClient("Hi!");
                    _speechService.MicrophoneRecognition("Hi!");

                }
            }
            else
            {
                _numberOfDetection = 0;
            }
            return View("Camera");
        }

        private byte[] String_To_Bytes2(string strInput)
        {
            int numBytes = (strInput.Length) / 2;
            byte[] bytes = new byte[numBytes];
            for (int x = 0; x < numBytes; ++x)
            {
                bytes[x] = Convert.ToByte(strInput.Substring(x * 2, 2), 16);
            }
            return bytes;
        }
    }
}