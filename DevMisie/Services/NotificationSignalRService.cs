﻿using DevMisie.Clients;
using DevMisie.Hubs;
using Microsoft.AspNet.SignalR;

namespace DevMisie.Services
{
    public class NotificationSignalRService : INotificationSignalRService
    {
        private IHubContext<IClient> BotHub => GlobalHost.ConnectionManager.GetHubContext<BotHub, IClient>();

        public void BotSayToClient(string message)
        {
            BotHub.Clients.All.BotSayToClient(message);
        }

    }
}