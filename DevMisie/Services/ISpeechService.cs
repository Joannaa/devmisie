﻿namespace DevMisie.Services
{
    public interface ISpeechService
    {
        void MicrophoneRecognition(string message);
    }
}
