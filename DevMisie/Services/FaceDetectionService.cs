﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.ProjectOxford.Face;
using Microsoft.ProjectOxford.Face.Contract;

namespace DevMisie.Services
{
    public class FaceDetectionService : IFaceDetectionService
    {
        public readonly IFaceServiceClient FaceServiceClient = new FaceServiceClient("e3dd86694c194dbc9f8473e1ca276e5e");
        public async Task<bool> DetectFace(string path)
        {
            try
            {
                using (Stream imageFileStream = File.OpenRead(path))
                {
                    var faces = await FaceServiceClient.DetectAsync(imageFileStream);
                    var numberOfFaces = faces.Length;
                    if (numberOfFaces > 0)
                    {
                        return true;
                    }
                }
            }
            catch (Exception)
            {
                //ignore
            }
            return false;
        }
    }
}