﻿using System.Threading.Tasks;

namespace DevMisie.Services
{
    public interface IFaceDetectionService
    {
        Task<bool> DetectFace(string path);
    }
}
