using System;
using Microsoft.Practices.Unity;
using System.Web.Http;
using DevMisie.Hubs;
using DevMisie.Services;
using Microsoft.ProjectOxford.Face;
using Unity.WebApi;

namespace DevMisie
{
    public static class UnityConfig
    {
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        public static void RegisterTypes(IUnityContainer container)
        {
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            container.RegisterType<IFaceDetectionService,FaceDetectionService>();
            container.RegisterType<INotificationSignalRService, NotificationSignalRService>();
            container.RegisterType<ISpeechService,SpeechService>();
            container.RegisterType<BotHub>(new InjectionFactory(CreateBotHub));

        }
        private static object CreateBotHub(IUnityContainer p)
        {
            var botHub = new BotHub(p.Resolve<ISpeechService>());
            return botHub;
        }
    }
}