﻿using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Security.DataProtection;
using Owin;

[assembly: OwinStartupAttribute(typeof(DevMisie.Startup))]
namespace DevMisie
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            var configuration = new HubConfiguration();
            app.MapSignalR("/signalr", configuration);
        }
    }
}