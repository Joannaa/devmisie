﻿using System.IO;

namespace DevMisie.Clients
{
    public interface IClient
    {
        void BotSayToClient(string msg);
        void BotSendAudoToClient(byte[] audio);
    }
}
